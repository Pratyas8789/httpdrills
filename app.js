const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require('uuid');

const PORT = process.env.PORT || 7000;
const HOST = 'localhost';

const server = http.createServer((req, res) => {
    if (req.url === "/html") {
        fs.readFile('index.html', 'utf8', (error, data) => {
            if (error) {
                res.writeHead(404, { "Content-Type": "text/html" });
                res.write("Sorry can't read html file!");
                res.end();
            } else {
                res.write(data);
                res.end();
            }
        });
    }
    else if (req.url === "/json") {
        fs.readFile('data.json', 'utf8', (error, data) => {
            if (error) {
                res.writeHead(404, { "Content-Type": "text/html" });
                res.write("Sorry can't read json file!!");
                res.end();
            } else {
                res.write(data);
                res.end();
            }
        });
    }
    else if (req.url === "/uuid") {
        let randomUUID = {
            uuid: uuidv4()
        };
        res.write(JSON.stringify(randomUUID));
        res.end();
    }
    else if (req.url.substring(0, 2) == "/s") {
        const statusCode = req.url.substring(8);
        let statusMsg = {};
        const statusCodeData = http.STATUS_CODES
        if (statusCodeData.hasOwnProperty(statusCode)) {
            statusMsg = {
                [statusCode]: statusCodeData[statusCode]
            };
        }
        else {
            res.writeHead(404, { "Content-Type": "text/html" });
            statusMsg = {
                "error": "Sorry this is not valid status code!!!"
            }
        }
        res.write(JSON.stringify(statusMsg));
        res.end();
    }
    else if (req.url.substring(0, 2) === "/d") {
        let delay = parseInt(req.url.substring(7, 8));
        if (isNaN(delay) || delay < 0) {
            res.writeHead(400, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "error": "Not a valid time"
            }));
        }
        else {
            setTimeout(() => {
                res.write(JSON.stringify({
                    "delay": `${delay} seconds`
                }));
                res.end();
            }, delay * 1000);
        }
    }
    else {
        res.write("url not found");
        res.end();
    }
});
server.listen(PORT, HOST, () => {
    console.log('server running at http://localhost:7000');
});